# Introduction

**FreeCam** is a simple squirrel **client-side** script which allows the client to toggle into free camera mode.  
The movement and the basic idea of this script is inspired by the free camera mode from Half Life game.  

## Supported g2o client version

You can download specific script version for g2o client from [here](../../releases)

## How to install?

1.Clone or download the repository source code  
2.Extract the code whenether you like in your g2o_server directory  
3.Add this line to your XML loading section  

**NOTE** That you have to edit the example path to match with your script location.

```xml
<script src="path/to/script/freecam.nut" side="client" />
```

## Usage example
```js
// client-side
addEventHandler("onCommand", function(cmd, params)
{
    // This command will toggle the free cam mode

	if (cmd == "freecam")
		enableFreeCam(!isFreeCamEnabled())
})

```
